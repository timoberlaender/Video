package de.grogra.video.ui;

import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * This {@code ChangeModelListener} is used to make sure SpinnerModels does not
 * exceed the limits.
 * 
 * @author Dominick Leppich
 *
 */
public class ChangeModelListener implements ChangeListener {
	private SpinnerNumberModel model;
	
	// ------------------------------------------------------------

	public ChangeModelListener(SpinnerNumberModel model) {
		this.model = model;
	}
	
	// ------------------------------------------------------------

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void stateChanged(ChangeEvent arg0) {
		int value = model.getNumber().intValue();
		Comparable min = model.getMinimum();
		Comparable max = model.getMaximum();

		if (min.compareTo(value) > 0)
			model.setValue(min);
		else if (max.compareTo(value) < 0)
			model.setValue(max);
	}
}
