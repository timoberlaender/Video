package de.grogra.video.simulation;

/**
 * A {@code SimulationMethod} is an abstract class for simulating a model.
 * 
 * @author Dominick Leppich
 *
 */
public abstract class SimulationMethod {
	/**
	 * Execute the simulation method.
	 */
	public abstract void execute();

	/**
	 * Get the name of the simulation method instance.
	 * 
	 * @return Name
	 */
	public abstract String getName();

	/**
	 * The {@link #toString()} method returns the name of the simulation method in
	 * order to correctly display it on the ui.
	 * 
	 * @return Name of the image provider
	 */
	public final String toString() {
		return getName();
	}
}
