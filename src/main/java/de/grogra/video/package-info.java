/**
 * This package contains the basic classes which are used from the outside of
 * the plugin.
 * 
 * @author Dominick Leppich
 * 
 */
package de.grogra.video;