package de.grogra.video.export;

import java.io.File;
import java.io.IOException;
import java.util.List;

import de.grogra.video.model.ImageSequence;
import de.grogra.video.model.ImageSequenceView;
import de.grogra.video.util.ProgressObservable;

/**
 * This abstract class represents objects capable of exporting videos. While
 * generating the video, the progress can be observed registering an observer.
 * 
 * @author Dominick Leppich
 *
 */
public abstract class VideoExporter extends ProgressObservable {
	/**
	 * Create the video with a given {@link ImageSequence} (given by its
	 * {@link ImageSequenceView}), the {@link VideoSettings} and the output
	 * {@link File}.
	 * 
	 * @param view
	 *            - {@link ImageSequenceView} of the image sequence
	 * @param settings
	 *            - {@link VideoSettings} of the video
	 * @param file
	 *            - Output file
	 * @throws IOException
	 *             if the video creation fails
	 */
	public abstract void createVideo(ImageSequenceView view, VideoSettings settings, File file) throws IOException;

	/**
	 * Get a list of possible file formats available with the video exporter.
	 * 
	 * @return {@link List} of {@link FileFormat}s
	 */
	public abstract List<FileFormat> getFileFormats();
}
