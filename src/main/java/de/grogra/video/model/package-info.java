/**
 * Classes in this package represent the data of the plugin, the
 * {@link ImageSequence} and the {@link VideoImage}s inside it.
 * 
 * @author Dominick Leppich
 * 
 */
package de.grogra.video.model;