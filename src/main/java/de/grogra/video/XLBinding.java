package de.grogra.video;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import de.grogra.pf.ui.Workbench;
import de.grogra.video.connector.VideoPluginConnector;
import de.grogra.video.export.ExportJob;
import de.grogra.video.export.VideoExporter;
import de.grogra.video.export.VideoSettings;
import de.grogra.video.interpolation.InterpolationJob;
import de.grogra.video.interpolation.InterpolationStrategy;
import de.grogra.video.model.ImageSequence;
import de.grogra.video.model.ImageSequenceControl;
import de.grogra.video.model.ImageSequenceView;
import de.grogra.video.render.ImageProvider;
import de.grogra.video.render.RenderJob;
import de.grogra.video.util.CommandLineProgress;
import de.grogra.video.util.Job;
import de.grogra.video.util.JobListener;
import de.grogra.video.util.Worker;

/**
 * The {@code XLBinding} class provides access to the plugin features throw
 * static methods. This class has a static {@link ImageSequence} member, which
 * can be modified by the method calls.
 * 
 * Using a static import the methods in this class can be used inside of XL code
 * in a GroIMP project.
 * 
 * @author Dominick Leppich
 *
 */
public class XLBinding {
	public static final int DEFAULT_OUTPUT_FPS = 25;

	private static Worker worker;
	private static CommandLineProgress progress;
	private static VideoPluginConnector connector;
	private static VideoExporter exporter;
	private static HashMap<String, ImageProvider> providers;
	private static HashMap<String, InterpolationStrategy> interpolators;

	private static ImageSequence images;
	private static ImageSequenceView view;
	private static ImageSequenceControl control;

	// ------------------------------------------------------------

	/**
	 * Initialize the static members of this class in order to work, when the
	 * corresponding methods are called.
	 */
	static {
		// Create a worker
		worker = Worker.instance();

		// Create a command line progress object and bind the worker to it
		progress = new CommandLineProgress(null, 50, '-', '#', "[", "]", true);
		worker.addJobListener(new JobListener() {
			@Override
			public void startJob(Job job) {
				job.addObserver(progress);
			}

			@Override
			public void finishedWork() {
			}

			@Override
			public void endJob(Job job) {
				job.deleteObserver(progress);
			}
		});

		// Create connector and exporter objects (using the default implementations)
		connector = VideoPlugin.createDefaultConnector(Workbench.current());
		exporter = VideoPlugin.createDefaultExporter();

		// Create a hash map for image providers and interpolators, to use them by name
		// later
		providers = new HashMap<>();
		for (ImageProvider ip : connector.getImageProviders())
			providers.put(ip.getName(), ip);

		interpolators = new HashMap<>();
		for (InterpolationStrategy igs : VideoPlugin.getInterpolationStrategies())
			interpolators.put(igs.getName(), igs);

		// Init the sequence
		images = new ImageSequence();
		view = images.view();
		control = images.control();
	}

	// ------------------------------------------------------------

	/**
	 * Return the current size of the {@link ImageSequence}.
	 * 
	 * @return The size of the {@link ImageSequence}
	 */
	public synchronized static int size() {
		return images.size();
	}

	/**
	 * Clears the {@link ImageSequence} (delete all collected images).
	 */
	public synchronized static void clear() {
		control.clear();
	}

	/**
	 * Render an image using the provided {@link ImageProvider} (by its name) with
	 * the specified dimension and add the image to the {@link ImageSequence}.
	 * 
	 * @param provider
	 *            - Name of the {@link ImageProvider}
	 * @param xDim
	 *            - Width of the image
	 * @param yDim
	 *            - Height of the image
	 */
	public synchronized static void renderImage(String provider, int xDim, int yDim) {
		if (!providers.containsKey(provider)) {
			VideoPlugin.handleError(new XLBindingException("ImageProvider \"" + provider + "\" does not exist"));
			return;
		}
		worker.addJob(new RenderJob(providers.get(provider), new Dimension(xDim, yDim), control));
		worker.waitUntilFinished();
	}

	/**
	 * Interpolate between all images using the provided strategy (by its name).
	 * 
	 * @param strategy
	 *            - Name of the {@link InterpolationStrategy}
	 * @param count
	 *            - Number of new images to compute between two images
	 */
	public synchronized static void interpolate(String strategy, int count) {
		interpolate(strategy, count, 1, view.size());
	}

	/**
	 * Interpolate between all images between {@code startIndex} and
	 * {@code endIndex} using the provided strategy (by its name). The indices are
	 * 1-based.
	 * 
	 * @param strategy
	 *            - Name of the {@link InterpolationStrategy}
	 * @param count
	 *            - Number of new images to compute between two images
	 * @param startIndex
	 *            - First image to interpolate between (1-based)
	 * @param endIndex
	 *            - Last image to interpolate between (1-based)
	 */
	public synchronized static void interpolate(String strategy, int count, int startIndex, int endIndex) {
		if (!interpolators.containsKey(strategy)) {
			VideoPlugin
					.handleError(new XLBindingException("InterpolationStrategy \"" + strategy + "\" does not exist"));
			return;
		}
		worker.addJob(
				new InterpolationJob(interpolators.get(strategy), view, control, count, startIndex - 1, endIndex - 1));
		worker.waitUntilFinished();
	}

	/**
	 * Export the {@link ImageSequence} to a file with given input fps values.
	 * 
	 * @param fileName
	 *            - File name where the image will be exported
	 * @param inputFps
	 *            - Input fps of the video
	 * @throws IOException
	 *             if the process of video creation fails
	 */
	public synchronized static void exportVideo(String fileName, int inputFps) throws IOException {
		exportVideo(fileName, inputFps, DEFAULT_OUTPUT_FPS);
	}

	/**
	 * Export the {@link ImageSequence} to a file with given input and output fps
	 * values.
	 * 
	 * @param fileName
	 *            - File name where the image will be exported
	 * @param inputFps
	 *            - Input fps of the video
	 * @param outputFps
	 *            - Output fps of the video
	 * @throws IOException
	 *             if the process of video creation fails
	 */
	public synchronized static void exportVideo(String fileName, int inputFps, int outputFps) throws IOException {
		worker.addJob(new ExportJob(view, exporter, new VideoSettings(inputFps, outputFps), new File(fileName)));
		worker.waitUntilFinished();
	}

	/**
	 * Print all available {@link ImageProvider}s to the command line.
	 */
	public synchronized static void printImageProviders() {
		for (String s : providers.keySet())
			System.out.println(s);
	}

	/**
	 * Print all available {@link InterpolationStrategy}s to the command line.
	 */
	public synchronized static void printInterpolationStrategies() {
		for (String s : interpolators.keySet())
			System.out.println(s);
	}
}
