package de.grogra.video.render;

import java.awt.Dimension;
import java.io.IOException;

import de.grogra.video.model.VideoImage;
import de.grogra.video.util.ProgressObservable;

/**
 * Object which is capable of creating {@link VideoImage} instances showing the
 * current scene.
 * 
 * @author Dominick Leppich
 *
 */
public abstract class ImageProvider extends ProgressObservable {
	/**
	 * Creates an image with the given dimension.
	 * 
	 * @param dimension
	 *            - Dimension to render with
	 * @return Created image as {@link VideoImage}
	 * @throws InterruptedException
	 *             if the image creation is interrupted
	 * @throws IOException
	 *             if creating a {@link VideoImage} fails
	 */
	public abstract VideoImage createImage(Dimension dimension) throws IOException, InterruptedException;

	/**
	 * Get the name of the image generating instance.
	 * 
	 * @return Name
	 */
	public abstract String getName();

	/**
	 * The {@link #toString()} method returns the name of the image provider in
	 * order to correctly display it on the ui.
	 * 
	 * @return Name of the image provider
	 */
	public final String toString() {
		return getName();
	}
}
