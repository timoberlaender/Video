package de.grogra.video;

/**
 * This exception is thrown in methods of the {@link XLBinding} class.
 * 
 * @author Dominick Leppich
 *
 */
public class XLBindingException extends Exception {
	private static final long serialVersionUID = 1L;

	// ------------------------------------------------------------

	public XLBindingException(String msg) {
		super(msg);
	}

	public XLBindingException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
