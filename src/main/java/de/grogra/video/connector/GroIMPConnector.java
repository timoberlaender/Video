package de.grogra.video.connector;

import java.util.ArrayList;
import java.util.List;

import de.grogra.graph.impl.Node.NType;
import de.grogra.imp.Renderer;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.expr.Expression;
import de.grogra.pf.ui.Workbench;
import de.grogra.reflect.Method;
import de.grogra.rgg.RGG;
import de.grogra.util.StringMap;
import de.grogra.video.render.ImageProvider;
import de.grogra.video.render.RendererAdaptor;
import de.grogra.video.render.SceneSnapshotMaker;
import de.grogra.video.simulation.RGGMethodAdaptor;
import de.grogra.video.simulation.SimulationMethod;

/**
 * The {@code GroIMPConnector} is the GroIMP implementation of the
 * {@link VideoPluginConnector} interface and gives access to GroIMPs
 * {@link ImageProvider}s and {@link SimulationMethod}s.
 * 
 * @author Dominick Leppich
 * 
 */
public class GroIMPConnector implements VideoPluginConnector {
	private Workbench workbench;

	// ------------------------------------------------------------

	/**
	 * Default ctr.
	 * 
	 * @param workbench
	 *            - the GroIMP {@link Workbench} for the current project
	 */
	public GroIMPConnector(Workbench workbench) {
		this.workbench = workbench;
	}

	// ------------------------------------------------------------

	public Workbench getWorkbench() {
		return workbench;
	}
	
	/**
	 * <p>
	 * {@inheritDoc}
	 * </p>
	 * 
	 * <p>
	 * Available {@link ImageProvider}s are the GroIMP {@link Renderer} and the
	 * {@link SceneSnapshotMaker}. The {@link Renderer} are wrapped with
	 * {@link RendererAdaptor} in order to match the abstract class
	 * {@link ImageProvider}.
	 * </p>
	 */
	@Override
	public List<ImageProvider> getImageProviders() {
		// Initialize empty list of renderers
		ArrayList<ImageProvider> list = new ArrayList<>();

		// Add GroIMP Renderer instances, which are added to the registry
		for (Item i = (Item) workbench.getRegistry().getRootRegistry().getItem("/renderers/3d")
				.getBranch(); i != null; i = (Item) i.getSuccessor()) {

			// Check if item is an "Expression"
			if (i instanceof Expression) {
				Object r = ((Expression) i).evaluate(workbench, new StringMap());
				// Add renderers only
				if (r instanceof Renderer)
					list.add(new RendererAdaptor(workbench, i));
			}
		}

		// Add the snapshot maker
		list.add(new SceneSnapshotMaker(workbench));

		return list;
	}

	/**
	 * <p>
	 * {@inheritDoc}
	 * </p>
	 * 
	 * <p>
	 * The possible methods are available in the {@link RGG} object of the project.
	 * The {@link RGGMethodAdaptor} makes them executable for the plugin.
	 * </p>
	 */
	@Override
	public List<SimulationMethod> getSimulationMethods() {
		// Initialize empty list of simulation methods
		ArrayList<SimulationMethod> list = new ArrayList<>();

		// Get main RGG
		if (workbench == null)
			return null;
		RGG mainRGG = RGG.getMainRGG(workbench);

		if (mainRGG == null)
			return null;

		NType n = mainRGG.getNType();
		int declaredMethodCount = n.getDeclaredMethodCount();

		for (int i = 0; i < declaredMethodCount; i++) {
			Method m = n.getDeclaredMethod(i);
			if (RGG.isTransformationMethod(m))
				list.add(new RGGMethodAdaptor(workbench, mainRGG, m));
		}

		return list;
	}
}
