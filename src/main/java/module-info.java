module video {
	exports de.grogra.video.interpolation;
	exports de.grogra.video.connector;
	exports de.grogra.video.render;
	exports de.grogra.video.ui;
	exports de.grogra.video.test;
	exports de.grogra.video.util;
	exports de.grogra.video;
	exports de.grogra.video.model;
	exports de.grogra.video.simulation;
	exports de.grogra.video.export;

	requires graph;
	requires imp;
	requires imp3d;
	requires platform;
	requires platform.core;
	requires platform.swing;
	requires rgg;
	requires utilities;
	requires xl.core;
	requires java.datatransfer;
	requires java.desktop;
}